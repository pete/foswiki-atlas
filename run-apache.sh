#!/bin/bash
if [[ -z "$HOSTNAME_FQDN" ]]; then
  export HOSTNAME_FQDN="${NAMESPACE}.web.cern.ch"
fi


#copy the distribution from tmp to the permanent storage


#
# Ensure that assigned uid has entry in /etc/passwd.

if [ `id -u` -ge 10000 ]; then
cat /etc/passwd | sed -e "s/^$NB_USER:/builder:/" > /tmp/passwd
echo "foswiki:x:`id -u`:`id -g`:Dummy account for certain commands:/home/foswiki:/bin/bash" >> /tmp/passwd
cat /tmp/passwd > /etc/passwd
rm /tmp/passwd
fi
#
#Install the Openid contrib

cd /foswiki/Foswiki-2.1.6
#curl -k -o OpenIDLoginContrib.tgz https://foswiki.org/pub/Extensions/OpenIDLoginContrib/OpenIDLoginContrib.tgz
#gunzip -c OpenIDLoginContrib.tgz | tar -xvf -

#rm -rf *Plugin_installer *.tgz
#rm -rf *Contrib_installer *.tgz

#Change the DefaultUrlHost appropriately
#cp /tmp/LocalSite.cfg /foswiki/prod/lib/LocalSite.cfg
sed "s/foswikiadm.cern.ch/${NAMESPACE}.web.cern.ch/" -i /foswiki/prod/lib/LocalSite.cfg
sed "s/CLIENTIDHOLDER/${NAMESPACE}/" -i /foswiki/prod/lib/LocalSite.cfg
sed "s/CLIENTSECRETHOLDER/${OIDC_CLIENT_SECRET}/" -i /foswiki/prod/lib/LocalSite.cfg


#Set the permissions
chmod -R 775 /foswiki/prod/data
chmod -R 775 /foswiki/prod/pub
chmod -R 775 /foswiki/prod/lib
chmod -R 775 /foswiki/prod/working


# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.
rm -rf /run/httpd/*

#copy config files from /tmp to /etc/httpd/conf.d/

#Sed config file to change ports
sed "s/OIDC_CLIENT_SECRET/${OIDC_CLIENT_SECRET}/" -i /etc/httpd/conf.d/openidc.conf

sed "s/CLIENTIDHOLDER/${NAMESPACE}/" -i /var/www/cgi-bin/protected/get_groups.pl && \
sed "s/CLIENTSECRETHOLDER/${OIDC_CLIENT_SECRET}/" -i /var/www/cgi-bin/protected/get_groups.pl

echo "${NAMESPACE}"

echo "FINISHED"


exec /usr/sbin/httpd -D FOREGROUND
