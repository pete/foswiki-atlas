FROM cern/cc7-base
MAINTAINER Peter Jones <peter.l.jones@cern.ch>
LABEL description="A basic Apache HTTP server container on RHEL 7"



RUN yum -y update; yum clean all
RUN yum -y install httpd; yum clean all
RUN yum install -y  mod_auth_openidc enscript rsync htmldoc pandoc sendmail lynx latex2html rcs dvipng vim graphviz cairo doxygen gnuplot wget gcc mod_perl gettext cpanminus libxml2-devel java-1.8.0-openjdk.x86_64 && \
    yum install -y perl \
    GraphicsMagick-perl \
    ImageMagick-perl \
    perl-Apache-LogFormat-Compiler   \
    perl-Archive-Zip   \
    perl-AuthCAS   \
    perl-BerkeleyDB   \
    perl-Cache-Cache   \
    perl-Carp-Assert   \
    perl-Carp-Clan   \
    perl-CGI-Session   \
    perl-Class-Accessor   \
    perl-Config   \
    perl-Convert-PEM   \
    perl-CPAN   \
    perl-Crypt-SMIME   \
    perl-Data-Dumper   \
    perl-Date-Calc   \
    perl-Date-Manip   \
    perl-DateTime   \
    perl-DBD-MySQL   \
    perl-DBD-Pg   \
    perl-DBD-SQLite   \
    perl-DB_File   \
    perl-DB_File-Lock   \
    perl-DBI   \
    perl-Devel-OverloadInfo   \
    perl-Devel-Symdump   \
    perl-Digest-Perl-MD5   \
    perl-Email-MIME   \
    perl-FCGI   \
    perl-FCGI-ProcManager   \
    perl-File-Copy-Recursive   \
    perl-File-Remove   \
    perl-File-Slurp   \
    perl-Filesys-Notify-Simple   \
    perl-File-Temp   \
    perl-File-Which   \
    perl-GD   \
    perl-GSSAPI   \
    perl-Hash-Merge-Simple   \
    perl-Hash-MultiValue   \
    perl-Image-Info   \
    perl-IO-Socket-INET6   \
    perl-IPC-Run   \
    perl-JSON   \
    perl-JSON-XS   \
    perl-LDAP   \
    perl-libwww-perl   \
    perl-Locale-Codes   \
    perl-Locale-Maketext-Lexicon   \
    perl-LWP-Protocol-https   \
    perl-Module-Install   \
    perl-Module-Pluggable   \
    perl-Moose   \
    perl-parent   \
    perl-Path-Tiny   \
    perl-Sereal   \
    perl-Sort-Versions   \
    perl-Spreadsheet-ParseExcel   \
    perl-Spreadsheet-XLSX   \
    perl-Stream-Buffered   \
    perl-String-RewritePrefix   \
    perl-String-ShellQuote   \
    perl-Sys-SigAction   \
    perl-Task-Weaken   \
    perl-Test-Class   \
    perl-Test-Deep   \
    perl-Test-Exception   \
    perl-Test-LeakTrace   \
    perl-Test-Warn   \
    perl-Text-Soundex   \
    perl-Text-Markdown \
    perl-Text-Unidecode   \
    perl-Time-Duration   \
    perl-Time-Duration-Parse   \
    perl-Time-Local   \
    perl-Time-modules   \
    perl-Time-ParseDate   \
    perl-Try-Tiny   \
    perl-Unicode-Map   \
    perl-Unicode-Map8   \
    perl-Unicode-MapUTF8   \
    perl-Unicode-String   \
    perl-WWW-Mechanize   \
    perl-WWW-Curl   \
    perl-XML-Generator   \
    perl-XML-Parser   \
    perl-XML-Simple \
    perl-XML-Tidy   \
    perl-XML-Writer   \
    perl-XML-XPath   \
    perl-YAML   \
    perl-YAML-Tiny   && \
    yum install -y git phonebook && \
    yum clean all -y

RUN cpanm Algorithm::Diff \
 Algorithm::Diff::XS \
 Archive::Tar \
 Authen::SASL \
 Bit::Vector \
 CGI \
 CGI::Carp \
 CGI::Cookie \
 Crypt::JWT \
 Crypt::Misc \
 Crypt::PasswdMD5 \
 Crypt::Random \
 Dancer2 \
 DateTime::Format::XSD \
 Digest::base \
 Digest::SHA \
 Email::Address \
 Email::MIME \
 Encode \
 Error \
 File::Copy \
 File::Copy::Recursive \
 File::Find \
 File::Find::Rule \
 FileHandle \
 File::Spec \
 HTML::CalendarMonthSimple \
 HTML::Entities \
 HTML::Parser \
 HTML::Tree \
 IO::File \
 IO::Socket::IP \
 IO::Socket::SSL \
 JSON \
 XML::LibXML \
 Locale::Maketext \
 Locale::Maketext::Lexicon \
 Locale::Msgfmt \
 LWP \
 LWP::Protocol::https \
 Math::Pari \
 MIME::Base64 \
 Moo \
 MooseX \
 MooseX::Types::Common \
 MooseX::Types::DateTime \
 MooseX::Types::URI \
 MooX::Types::MooseLike \
 Net::SMTP \
 Number::Compare \
 Pod::Coverage \
 Pod::Parser \
 Sub::Uplevel \
 Sub::Exporter::ForMethods \
 Test::Deep \
 Test::Inter \
 Test::Pod \
 Test::Pod::Coverage \
 Text::Glob \
 Types::Standard \
 Type::Tiny \
 Try::Tiny \
 URI \
 version \
 XML::CanonicalizeXML \
 XML::Easy

#RUN cpanm --uninstall Cpanel::JSON::XS

RUN echo "<h1>Hello World</h1>Hello this is Foswiki<p>$(date)" >> /var/www/html/index.html


RUN mkdir -p /usr/local/httpd/ /etc/httpd/conf.d/configurable/ /etc/httpd/conf.d/vhost/ /run/httpd/ /etc/httpd/logs /tmp/apache /tmp/foswiki /var/log/oidc /foswiki/tmp && \
    chmod -R 777 /usr/local/httpd/ /etc/httpd/ /run/httpd/ /var/log/httpd/ /var/run/ && \
    chown -R apache:apache /etc/httpd/logs/ /run/httpd/ /var/log/oidc && \
    chmod -R 755 /var/www/html && \
    mkdir /opt/foswiki && \
    chmod -R 777 /opt/foswiki && \
    chmod -R 777 /tmp && \
    chmod -R 777 /tmp/apache && \
    chmod -R 777 /tmp/foswiki && \
    chmod -R 777 /foswiki/tmp && \
    chmod -R 777  /var/log/oidc/ && \
    ln -s /foswiki/Foswiki-2.1.6 /opt/foswiki/Foswiki-2.1.6 && \
    ln -s /foswiki/prod /var/www/html/foswiki && \
    mkdir /var/www/html/protected && \ 
    mkdir /var/www/cgi-bin/protected && \
    chmod -R 777  /var/www/cgi-bin/protected/



# Set Listen to 8080 as openshift wont allow listening from port 80
RUN   sed -i "s|Listen 80|Listen 8080|g" /etc/httpd/conf/httpd.conf

EXPOSE 8080

# Simple startup script to avoid some issues observed with container restart 
ADD run-apache.sh /run-apache.sh
COPY html_files/helloworld.pl /var/www/cgi-bin/
COPY html_files/helloworld.pl /var/www/cgi-bin/protected/
COPY html_files/get_groups.pl /var/www/cgi-bin/protected/
COPY conf_files/openidc.conf /etc/httpd/conf.d/openidc.conf
COPY conf_files/httpd10.conf /etc/httpd/conf.d/httpd10.conf
COPY conf_files/10-foswiki.conf /etc/httpd/conf.d/10-foswiki.conf
COPY html_files/protected.html /var/www/html/protected/index.html
COPY html_files/perl.pl /var/www/cgi-bin/protected
COPY foswiki_config_files/LocalSite.cfg /tmp/LocalSite.cfg


RUN wget -q -O /foswiki/Foswiki-2.1.6.tar.gz --no-check-certificate https://twiki.cern.ch/twiki/pub/Foswiki/Lastest/Foswiki-2.1.6.tar.gz && \
        cd /foswiki && \
        tar zxvpf Foswiki-2.1.6.tar.gz && \
        ln -s Foswiki-2.1.6 prod


RUN chmod g+w /etc/passwd

RUN chmod -v +x /run-apache.sh

CMD ["/run-apache.sh"]


