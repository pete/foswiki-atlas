#!/usr/bin/perl
use strict;
use warnings;
use Crypt::Random;
use Crypt::JWT;
use LWP::UserAgent;
use MIME::Base64;
use JSON;use WWW::Curl::Easy;
use JSON::MaybeXS ();
use JSON::MaybeXS qw(encode_json decode_json);

my $debug = 1;
print &PrintHeader;
my @group_memberships=();
#necessary variables
#
my $server = "https://auth.cern.ch/auth/realms/cern";
my $token_endpoint = "protocol/openid-connect/token";
my $subject_token_type = "urn:ietf:params:oauth:token-type:access_token";
my $token_exchange_grant_type = "urn:ietf:params:oauth:grant-type:token-exchange";
# The next variables could also be parsed as arguments to the script
# 
my $client_id ="CLIENTIDHOLDER";
my $client_secret = "CLIENTSECRETHOLDER";
my $audience = "authorization-service-api";
#
my $url = 'https://auth.cern.ch/auth/realms/cern'.'/'.'protocol/openid-connect/token';

print "<h3>Step 1 Get the ENV OIDC variable</h3>\n" if $debug;


my $API_access_token;
my $OIDC_access_token = get_env_OIDC_access_token();
chomp($OIDC_access_token) if defined($OIDC_access_token);


print "<h3>OIDC_access_token</h3>$OIDC_access_token\n" if $debug;

print "<h3>Step 2 get the API access token</h3>\n" if $debug;
if (defined $OIDC_access_token){
    print "<h3>Passing the OIDC variable</h3>$OIDC_access_token\n" if $debug;
    $API_access_token = get_api_access_token($OIDC_access_token);
}
else{
    print "Error no OIDC_access_token found\n";
}



print "<h3>API_access_token</h3>$API_access_token\n" if $debug;


print "<h3>Step 3 run curl on the API access token</h3>\n" if $debug;

#$API_access_token = temp_api_access_token();
@group_memberships = get_curl_results($API_access_token);

foreach my $g (@group_memberships){
    print "<p>$g\n";
}

sub get_env_OIDC_access_token {
    if(defined $ENV{'OIDC_access_token'}) {
        return $ENV{'OIDC_access_token'};
    }
    else{
        return undef;
    }
       
}

sub get_api_access_token {
    my $access_token = $_[0];
    print "<h5>OIDC_access_token</h5>$access_token\n" if $debug;
    my $params = {
        client_id => $client_id,
        client_secret => $client_secret,
        grant_type => $token_exchange_grant_type,
        audience => $audience,
        subject_token => $access_token,
        subject_token_type => $subject_token_type
    };

    my $ua       = LWP::UserAgent->new();
    my $response = $ua->post( $url,  $params );
    my $content;
    my $api_access_token;
    my $exp_timestamp;
    if ($response->is_success) {
        $content  = $response->decoded_content();
        $api_access_token = extract_id_token($response->decoded_content);
        $exp_timestamp = extract_exp($response->decoded_content);
        print "<p>i Expiration dat of access_token is : $exp_timestamp<p>\n";
        return ($api_access_token);
    }
    else{
        return undef;
    }
}

sub get_curl_results {
    my $mytoken = $_[0]; 
    return undef if (!defined($mytoken));
    my @groups=();   
    my $curl = WWW::Curl::Easy->new;
    my @headers  = ("accept: */*", "Authorization: Bearer $mytoken");
    $curl->setopt(CURLOPT_HTTPHEADER, \@headers);
    $curl->setopt(CURLOPT_HEADER,1);
    $curl->setopt(CURLOPT_URL, 'https://authorization-service-api.web.cern.ch/api/v1.0/Group/my');
    my $response_body;
    my $response_data;
    my $response_header;

    $curl->setopt(CURLOPT_HEADERDATA,\$response_header);
    $curl->setopt(CURLOPT_WRITEDATA,\$response_body);


    my $retcode = $curl->perform; # Start the actual request
    # Looking at the results..e
    if ($retcode == 0) {
        #print("Transfer went ok\n");
        my $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
        my $header_size = $curl->getinfo(CURLINFO_HEADER_SIZE);
        $response_data = substr($response_body, $header_size);
        $response_data =~s/^\s*(.*?)\s*$/$1/; #trim
    } 
    else {
        # This didnt work - return empty groups list
        return (@groups);
    }

    my $mydecode = decode_json $response_data;
    
    for my $data( @{$mydecode->{data}} ){
        push(@groups,$data->{groupIdentifier});
        #print "<p>".$data->{groupIdentifier} . "\n";
    };
    return (@groups);

}

sub extract_id_token {
    my $bearer_token_data = JSON::decode_json(shift);
    return $bearer_token_data->{'access_token'};
}

sub extract_exp {
    use Data::Dumper;
    my $bearer_token_data = JSON::decode_json(shift);
    print Dumper $bearer_token_data;
    return $bearer_token_data->{'expires_in'};
}


sub PrintHeader {
  return "Content-type: text/html\n\n";
}

sub temp_api_access_token {

    my $t ="eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWYUQzRDBQUm5QVzB5MXpBLTBieVIxZkhsSFVqalNFZzAxNngyY3JjaHljIn0.eyJqdGkiOiJjNzYzMmExMC03ZmRmLTRlZjctYjFiMC03YTA1YzU3MjZ
jMDEiLCJleHAiOjE1NzY2ODE5MzEsIm5iZiI6MCwiaWF0IjoxNTc2NjgxMDMxLCJpc3MiOiJodHRwczovL2F1dGguY2Vybi5jaC9hdXRoL3JlYWxtcy9jZXJuIiwiYXVkIjoiYXV0aG9yaXphdGlvbi1zZXJ2aWNlLWFwaSIsInN1YiI6Ij
g0N2Q1MDdlLWQwNzQtNDI4OS05ZmI5LTljZGY2ODY0YTkxMCIsInR5cCI6IkJlYXJlciIsImF6cCI6ImF1dGhvcml6YXRpb24tc2VydmljZS1hcGkiLCJhdXRoX3RpbWUiOjE1NzY2Nzg2NzEsInNlc3Npb25fc3RhdGUiOiIzYmRlMjA1M
i1iYzg0LTRmYTQtOWViYy01ZGNkOGZmNDFhZDQiLCJhY3IiOiIwIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHBzOi8vYXV0aG9yaXphdGlvbi1zZXJ2aWNlLWFwaS53ZWIuY2Vybi5jaCJdLCJzY29wZSI6Im9pZGMtY2Vybi1wcm9maWxl
IG9pZGMtZW1haWwgb2lkYy1pZGVudGl0eS1wcm92aWRlciBvaWRjLWNlcm4tbG9naW4taW5mbyBvaWRjLWNsaWVudC1pZCIsInN1YiI6InBldGUiLCJpZGVudGl0eV9wcm92aWRlciI6ImYzNzAwZjdkLTg1YjUtNGIyOS1iNmI0LTk4NzU
yMmFjOWVhNiIsInJlc291cmNlX2FjY2VzcyI6eyJhdXRob3JpemF0aW9uLXNlcnZpY2UtYXBpIjp7InJvbGVzIjpbImdyb3Vwc191c2VyIiwiYXBwbGljYXRpb25fdXNlciIsImlkZW50aXR5X3VzZXIiXX19LCJjZXJuX3BlcnNvbl9pZC
I6NDAzNDAwLCJuYW1lIjoiUGV0ZSBKb25lcyIsInByZWZlcnJlZF91c2VybmFtZSI6InBldGUiLCJnaXZlbl9uYW1lIjoiUGV0ZSIsImNlcm5fcm9sZXMiOlsiZ3JvdXBzX3VzZXIiLCJhcHBsaWNhdGlvbl91c2VyIiwiaWRlbnRpdHlfd
XNlciJdLCJjZXJuX3ByZWZlcnJlZF9sYW5ndWFnZSI6IkVOIiwiZmFtaWx5X25hbWUiOiJKb25lcyIsImVtYWlsIjoicGV0ZXIubC5qb25lc0BjZXJuLmNoIiwiY2Vybl91cG4iOiJwZXRlIn0.XuSNO_P3N928UWu0tSTrIhzhr9En7tok
OvCpz8e91WjF0yUa7l2dwDjaFCwgSk8rmZK16-sS7kNbzfW75ME5u7vpDyGeBhxWq7gsSiyjiZAK41707up4NLelYEb7JQ1x8xX_HSqY5QE7w_0PUetwoohDAoDz0j9_UohnLMu96H0xsOjo2wwrXLRSaqzX3vd1CaMbsK-IZQKp3qnKsBm
WIBBDxj2SyY11kL914wFNwjSm2dZa6sD0O1r0zuGRYKPk4O6SA36h0viA_LzNStgaSnt_D_Anb-41CJDNF9JYZL1QKcoUZOeavnjXG4ljCFz9WajtZ3OCyzf4CB9Slqqhmg";
}

