#!/usr/bin/perl

print "Content-type: text/html\n\n";
print "<pre>\n";

print "$ENV{'OIDC_access_token'}\n";

foreach $key (sort keys(%ENV)) {
  print "$key = $ENV{$key}<p>";
}
print "</pre>\n";
